app.factory('memoryGame',
    ['$timeout', '$interval',
        function ($timeout, $interval) {

            function capitalize (str) {
                return str.charAt(0).toUpperCase() + str.slice(1).toLowerCase();
            }

            function timer(t) {
                const minutes = Math.floor(t / 60)
                const seconds = t % 60
                return `${minutes <= 9 ? '0' + minutes : minutes}:${seconds <= 9 ? '0' + seconds : seconds}`
            }

            function shuffle(arr) {
                return arr.sort(function () { return 0.5 - Math.random() });
            }

            function uid(i) {
                return `a${i}`
            }

            function cardObj(id, uid, icon) {
                return {
                    id: id,
                    cardUID: uid,
                    icon,
                    flipped: false, hit: false, status: 'back'
                }
            }

            function generateCards(deck, quantity) {
                deck = shuffle(deck)
                quantity = quantity > deck.length ? deck.length : quantity
                const cards = []
                for (let i = 0; i < quantity; i++) {
                    const cardUID = uid(i)
                    const icon = deck[i].icon
                    cards.push(cardObj(i, cardUID, icon))
                    cards.push(cardObj(i + quantity, cardUID, icon))
                }
                return shuffle(cards)
            }

            class Game {

                constructor() {
                    this.reset()
                    this.onChange = () => { }
                    this.onFinish = () => { }
                    this.deck = [
                        { icon: 'fa-book-open' },
                        { icon: 'fa-user' },
                        { icon: 'fa-bullseye' },
                        { icon: 'fa-charging-station' },
                        { icon: 'fa-at' },
                        { icon: 'fa-balance-scale' },
                        { icon: 'fa-bath' },
                        { icon: 'fa-bolt' },
                        { icon: 'fa-burn' },
                        { icon: 'fa-cogs' },
                        { icon: 'fa-cut' },
                    ]
                }

                start() {
                    this.reset();
                    this.cards = generateCards(this.deck, 7)
                    this.maxScore = this.cards.length / 2
                    this.interval = $interval(() => {
                        this.time++
                        this.timer = timer(this.time)
                        this.onChange(this.getData())
                    }, 1000)
                }

                restart() {
                    $interval.cancel(this.interval);
                    this.start()
                }

                reset() {
                    this.won = false
                    this.score = 0
                    this.tried = 0
                    this.time = 0
                    this.timer = '00:00'
                    this.selected = []
                    this.cards = []
                    this.maxScore = 0
                    $interval.cancel(this.interval);
                }

                flipCard(card) {
                    if (card.hit || card.flipped || this.selected.length === 2) {
                        return
                    }

                    card.flipped = true
                    card.status = 'front'
                    this.selected.push(card)
                    if (this.selected.length == 2) {
                        this.compareCards()
                    }
                }

                compareCards() {
                    this.tried++
                    if (this.selected[0].cardUID === this.selected[1].cardUID) {
                        this.correct()
                    } else {
                        this.wrong()
                    }
                }

                hasWin() {
                    return this.score === this.maxScore
                }

                correct() {
                    this.score++
                    this.selected.forEach(card => {
                        card.flipped = true
                        card.hit = true
                        card.status = 'hit'
                    })
                    this.selected = []

                    if (this.hasWin()) {
                        $interval.cancel(this.interval);
                        this.onFinish(this.getData())
                    }
                }

                wrong() {
                    this.selected[0].status = 'failed'
                    this.selected[1].status = 'failed'
                    $timeout(() => {
                        this.selected.forEach(card => {
                            card.flipped = false
                            card.hit = false
                            card.status = 'back'
                        })
                        this.selected = []
                    }, 500)
                }

                getData() {
                    return {
                        time: this.time,
                        timer: timer(this.time),
                        selected: this.selected,
                        won: this.hasWin(),
                        tried: this.tried,
                        score: this.score,
                        maxScore: this.maxScore
                    }
                }

                on(eventType, callback) {
                    console.log(`on${capitalize(eventType)}`)
                    this[`on${capitalize(eventType)}`] = callback
                }
            }

            return Game
        }])


app.factory('ranking',
    ['localStorageService',
        function ($store) {

            if ($store.get('ranking') === null || $store.get('ranking') === undefined) {
                $store.set('ranking', [])
            }

            return {
                all: function () {
                    return $store.get('ranking')
                },
                add: function (name, data) {
                    const all = $store.get('ranking')
                    all.push({
                        name: name,
                        score: data.score,
                        maxScore: data.maxScore,
                        tried: data.tried,
                        time: data.time,
                        timer: data.timer,
                        date: (new Date()).toLocaleString()
                    })
                    $store.set('ranking', all)
                    return all
                },
                reset: function () {
                    $store.set('ranking', [])
                    return []
                }
            }
        }])