var app = angular.module("memoryGameApp", ['LocalStorageModule']);

app.controller('GameController',
    ['$scope', 'memoryGame', 'ranking', function ($scope, MemoryGame, ranking) {

        this.ranking = ranking.all()
        this.playerReady = false
        this.player = ''
        this.game = new MemoryGame()

        this.game.on('change', (data) => { })
        this.game.on('finish', (data) => {
            this.ranking = ranking.add(this.player, data)
            alert('You win!')
        })

        $scope.ready = (name) => {
            if (name === undefined) return;
            this.playerReady = true;
            this.player = name
        }

        $scope.start = () => {
            console.log(this)
            this.game.start()
        }

        $scope.restart = () => {
            console.log(this)
            this.game.restart()
        }

        $scope.turn = (card) => {
            this.game.flipCard(card)
        }

        $scope.changePlayer = () => {
            this.game.reset()
            this.playerReady = false;
        }

    }])