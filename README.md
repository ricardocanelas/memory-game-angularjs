The classic memory game. This is a simple app using AngularJS (1.7.2)

![screen](printscreen.jpg)

# Running locally

1. Clone this repository

2. Just open the index.html file in a browser 😁

(opening in a modern browser)